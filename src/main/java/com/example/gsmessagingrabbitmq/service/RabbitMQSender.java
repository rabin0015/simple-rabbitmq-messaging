package com.example.gsmessagingrabbitmq.service;

import com.example.gsmessagingrabbitmq.model.Employee;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RabbitMQSender {
    private final AmqpTemplate rabbitTemplate;

    private static String EXCHANGE="direct-exchange";
    private static String QUEUE = "usermsgQuer";
    private static String ROUTINGKEY = "userRounting";

    public void send(Employee employee) {
        rabbitTemplate.convertAndSend(EXCHANGE,ROUTINGKEY,employee);
        System.out.println("Send msg=" + employee);
    }
}
