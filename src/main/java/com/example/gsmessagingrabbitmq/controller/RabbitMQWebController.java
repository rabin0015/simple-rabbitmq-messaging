package com.example.gsmessagingrabbitmq.controller;

import com.example.gsmessagingrabbitmq.model.Employee;
import com.example.gsmessagingrabbitmq.service.RabbitMQSender;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/javainuse-rabbitmq/")
@AllArgsConstructor
@Slf4j
public class RabbitMQWebController {
    private final RabbitMQSender rabbitMQSender;
    private final RabbitTemplate rabbitTemplate;

    @GetMapping("/producer")
    public String producer(@RequestParam("empName") String empName, @RequestParam("empId") String empId) {
        Employee employee = new Employee();
        employee.setEmpId(empId);
        employee.setEmpName(empName);
        rabbitMQSender.send(employee);
        return "Message has been send to RabbitMQ JavaInUse Successfully!!";
    }
}
