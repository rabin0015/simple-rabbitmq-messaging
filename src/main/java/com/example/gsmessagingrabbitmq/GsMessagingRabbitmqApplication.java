package com.example.gsmessagingrabbitmq;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GsMessagingRabbitmqApplication {

	public static void main(String[] args) {
		SpringApplication.run(GsMessagingRabbitmqApplication.class, args);
	}

	@Bean
	public CachingConnectionFactory cachingConnectionFactory() {
		return new CachingConnectionFactory();
	}

}
